# ernie-xl

#### 介绍
基于ernie预训练模型和transformer xl思想做长文本分类

#### 安装教程

`python -m pip install paddlepaddle-gpu==2.3.2.post116 -f https://www.paddlepaddle.org.cn/whl/linux/mkl/avx/stable.html`

#### 使用说明

1. 基于循环机制和相对位置编码用于超长文本分类。

#### 参与贡献

1.  [Transformer-XL: Attentive Language Models Beyond a Fixed-Length Context](http://arxiv.org/abs/1901.02860)
